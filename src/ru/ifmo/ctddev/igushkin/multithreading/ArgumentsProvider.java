package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public interface ArgumentsProvider<X> {
    X makeArgument();
}
