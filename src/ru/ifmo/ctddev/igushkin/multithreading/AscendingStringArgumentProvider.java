package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public class AscendingStringArgumentProvider implements ArgumentsProvider<String> {
    int counter = 0;

    @Override
    public String makeArgument() {
        return Integer.toString(counter++)+" ";
    }
}
