package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public class Client<X, Y> implements Runnable {

    private final TaskRunner           runner;
    private final TaskFactory<X, Y>    taskFactory;
    private final ArgumentsProvider<Y> argumentsProvider;

    public Client(TaskRunner runner, TaskFactory<X, Y> taskFactory, ArgumentsProvider<Y> argumentsProvider) {
        this.runner = runner;
        this.taskFactory = taskFactory;
        this.argumentsProvider = argumentsProvider;
    }

    public void stop() {
        isStopping = true;
    }

    private boolean isStopping;

    @Override
    public void run() {
        isStopping = false;
        while (!isStopping) {
            System.out.println(runner.run(taskFactory.makeTask(),
                                          argumentsProvider.makeArgument()));
        }
    }
}
