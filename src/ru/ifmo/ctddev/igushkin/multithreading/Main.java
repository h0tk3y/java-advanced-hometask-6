package ru.ifmo.ctddev.igushkin.multithreading;

public class Main {

    public static void main(String[] args) {
        final TaskRunnerImpl runner = new TaskRunnerImpl(8);
        new Thread(new Client<>(runner,
                                new RandomRepeatTaskFactory(),
                                new AscendingStringArgumentProvider())).start();
        new Thread(new Client<>(runner,
                                new RandomRepeatTaskFactory(),
                                new DescendingStringArgumentsProvider())).start();
        new Thread(new Client<>(runner,
                                new RandomRepeatTaskFactory(),
                                () -> "a")).start();
    }
}
