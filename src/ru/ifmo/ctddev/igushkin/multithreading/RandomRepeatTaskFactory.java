package ru.ifmo.ctddev.igushkin.multithreading;

import java.util.Random;

/**
 * Created by Sergey.
 */
public class RandomRepeatTaskFactory implements TaskFactory<String, String> {
    Random rng = new Random();

    @Override
    public Task<String, String> makeTask() {
        final int times = rng.nextInt(10) + 1;
        return value -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i<times; ++i) {
                sb.append(value);
            }
            return sb.toString();
        };
    }
}
