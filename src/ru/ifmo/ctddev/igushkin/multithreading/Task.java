package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public interface Task<X, Y> {
    X run(Y value);
}
