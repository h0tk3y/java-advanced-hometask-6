package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public interface TaskFactory<X, Y> {
    Task<X, Y> makeTask();
}
