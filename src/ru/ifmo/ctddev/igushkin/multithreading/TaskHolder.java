package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */

public class TaskHolder<X, Y> {
    public final Task<X, Y> task;
    public final Y          argument;
    private      boolean    done;
    private      X          result;

    public TaskHolder(Task<X, Y> task, Y argument) {
        this.task = task;
        this.argument = argument;
    }

    public void complete() throws AlreadyDoneException {
        if (done)
            throw new AlreadyDoneException();
        done = true;
        this.result = task.run(argument);
    }

    public boolean isDone() { return done; }

    public X getResult() { return result; }

    public static class AlreadyDoneException extends Exception {
    }
}
