package ru.ifmo.ctddev.igushkin.multithreading;

/**
 * Created by Sergey.
 */
public interface TaskRunner {
    <X, Y> X run(Task<X, Y> task, Y value);
}
