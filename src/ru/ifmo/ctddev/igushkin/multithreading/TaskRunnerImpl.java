package ru.ifmo.ctddev.igushkin.multithreading;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Sergey.
 */
public class TaskRunnerImpl implements TaskRunner, Runnable {

    private final int threadsLimit;

    public void stop() { stopped = true; }

    @Override
    public void run() {
        if (!stopped) return;
        stopped = false;
        for (int i = 0; i < threadsLimit; ++i)
            new Thread(new ThreadWorker()).start();
    }

    private boolean stopped = true;

    public TaskRunnerImpl(int threadsLimit) {
        this.threadsLimit = threadsLimit;
        run();
    }

    private class ThreadWorker implements Runnable {

        @Override
        public void run() {
            while (!stopped) {
                TaskHolder h;
                synchronized (taskQueue) {
                    while (taskQueue.isEmpty()) {
                        try {
                            taskQueue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    h = taskQueue.poll();
                }
                synchronized (h) {
                    try {
                        h.complete();
                    } catch (TaskHolder.AlreadyDoneException e) {
                        e.printStackTrace();
                    }
                    h.notify();
                }
            }
        }
    }

    private final Queue<TaskHolder> taskQueue = new LinkedList<>();

    @Override
    public <X, Y> X run(Task<X, Y> task, Y value) {
        TaskHolder<X, Y> h = new TaskHolder<>(task, value);
        synchronized (taskQueue) {
            taskQueue.add(h);
            taskQueue.notify();
        }
        synchronized (h) {
            while (!h.isDone())
                try {
                    h.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            return h.getResult();
        }
    }


}
